public class Node {
    private int x;
    private int y;
    private int value;
    private int nbOfNode;
    private int[] neighbours;
    private boolean isStart, isEnd;
    private boolean locked;

    public Node(int x, int y, int value, int nbOfNode, int[] neighbours) {
        this.x = x;
        this.y = y;
        this.value = value;
        this.locked = false;
        this.isStart = false;
        this.isEnd = false;
        this.nbOfNode = nbOfNode;
        this.neighbours = neighbours;
    }

    public int getNbOfNode() {
        return nbOfNode;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int[] getNeighbours() {
        return neighbours;
    }

    public int getValue() {
        return value;
    }

    public boolean isStart() {
        return isStart;
    }

    public void setStart(boolean start) {
        isStart = start;
    }

    public boolean isEnd() {
        return isEnd;
    }

    public void setEnd(boolean end) {
        isEnd = end;
    }

    public boolean isLocked() {
        return locked;
    }

    public void setLocked(boolean locked) {
        this.locked = locked;
    }

    public void whoIam(){
        System.out.print("i'm the number " + this.nbOfNode + ", i'm located in " + x + " " + y + ", my value is " + value +" and my neighbours are : ");
        for (int i =0; i<neighbours.length; i++){
            if ( neighbours[i] == 0){
                continue;
            }
            System.out.print(neighbours[i] + " ");
        }

    }
}
