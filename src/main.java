import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.lang.System.*;

public class main {

    public static void main(String[] args) {
        char[][] adjacencyMatrix = {
                { 1, 1, 1, 1, 1, 1, 1, 'X', 1, 1, 1, 1, 1},
                { 1, 1, 1, 1, 1, 1, 1, 'X', 1, 1, 1, 1, 1},
                { 1, 1, 1, 1, 1, 1, 1, 'X', 1, 1, 1, 1, 1},
                { 1, 1, 'S', 1, 1, 1, 1, 'X', 1, 1, 1, 1, 1},
                { 1, 1, 1, 1, 1, 1, 1, 'X', 1, 'X', 'X', 'X', 'X'},
                { 1, 1, 1, 1, 1, 1, 1, 'X', 1, 1, 1, 1, 1},
                { 1, 1, 1, 1, 1, 1, 1, 'X', 1, 1, 1, 1, 1},
                { 1, 1, 2, 'X', 'X', 'X', 'X', 'X', 1, 1, 1, 1, 1},
                { 1, 1, 2, 1, 1, 1, 1, 'X', 1, 1, 1, 1, 1},
                { 1, 2, 1, 1, 2, 1, 1, 'X', 1, 1, 1, 1, 1},
                { 1, 1, 1, 2, 2, 2, 1, 'X', 1, 2, 2, 'E', 1},
                { 1, 1, 2, 2, 3, 2, 2, 'X', 1, 2, 3, 1, 1},
                { 1, 1, 1, 2, 2, 2, 1, 'X', 1, 1, 2, 1, 1},
                { 1, 1, 1, 1, 2, 1, 1, 'X', 1, 1, 1, 1, 1},
                { 1, 1, 1, 1, 1, 1, 1, 'X', 1, 1, 1, 1, 1},
                {'X', 'X', 'X', 1, 1, 1, 1, 'X', 1, 1, 1, 1, 1},
                { 1, 1, 1, 1, 1, 1, 1, 2, 1, 1, 1, 1, 1},
                { 1, 1, 1, 1, 1, 1, 3, 2, 1, 1, 1, 1, 1},
                { 1, 1, 1, 1, 2, 2, 1, 1, 1, 1, 1, 1, 1},
                { 1, 1, 1, 1, 1, 1, 1, 2, 1, 1, 1, 1, 1}
        };


        /**
         * adjacencyMatrix is the matrix directly from the "Excel"
         *
         * When retrieving the Excel spreadsheet, make sure it's in the right form
         */

        char[][] matrix = adjacencyMatrix;

        int[][] intMatrix;
        int maxY = matrix.length-1, maxX = matrix[0].length-1;

        /**
         * This change the array format to use it easly after
         * S = 0
         * E = 0
         * X = -1
         */

        intMatrix = parseMatrixCharToInt(matrix);

        List<Node> nodes = new ArrayList<Node>();

        /**
         * Create a List Of node with :
         *  - Its coordonnate X and Y
         *  - If the Node is the start point
         *  - If the Node is the end point
         *  - If the Node is a wall
         *  - The neighbour of the node
         */
        nodes = getNodes(maxX, maxY, intMatrix, matrix);

        /**
         * Get the starter node to use it in first
         */
        Optional<Node> starterNode = nodes.stream().filter(n -> n.isStart()).findFirst();

        if (!starterNode.isPresent()) {
            System.err.println("No starter point defined");
            exit(1);
        }

        /**
         * Graph is :
         *  - The parent Node
         *  - The destination Node
         *  - The prise (distance)
         *  - The parent graph id
         *  - Its id
         */
        boolean isFinished = false;
        List<Graph> graphTotal = new ArrayList<>();
        Node graphToProcess = starterNode.get();

        /**
         * From the start, the ditance is always 0
         */
        int distanceTotal = 0;

        Graph NextElementToProcess = null;
        int distanceMin = Integer.MAX_VALUE;

        /**
         * nodes = list of all points from the matrix
         */
        Id id = new Id();
        /**
         * The stater Graph is 0
         */
        id.setId(0);

        Graph lightestGraph = null;

        System.out.println("------------------ PLEASE WAIT ------------------");

        /**
         * The end is :
         *   - When all graph are locked
         *   - The first graph arraived to the end
         */
        
        while (!isFinished) {
            List<Graph> graphLocal = route(graphToProcess, distanceTotal, nodes, id, NextElementToProcess);

            graphTotal = Stream.concat(graphTotal.stream(), graphLocal.stream())
                    .collect(Collectors.toList());

            isFinished = true;

            for (Graph graph :
                    graphTotal) {

                if (!graph.isLocked()) {
                    isFinished = false;
                    lightestGraph = graph;
                    System.out.println("------------------ LIGTHEST ROAD FOUND ------------------");
                }

                if (graph.getToNode() == graphToProcess) {
                    graph.setLocked(true);
                }


                if(graph.getToNode().isEnd()){
                    isFinished = true;
                    lightestGraph = graph;
                    System.out.println("------------------ LIGTHEST ROAD FOUND ------------------");
                    break;
                }
            }

            /**
             * Take the graph who got the lightest total distance and who's not locked
             */

            List<Graph> listDeGraphNonTraites = graphTotal
                    .stream()
                    .filter(e -> e.isLocked() == false && e.getToNode().isLocked() == false)
                    .collect(Collectors.toList());

            NextElementToProcess = listDeGraphNonTraites
                    .stream()
                    .sorted(new Comparator<Graph>() {
                        @Override
                        public int compare(Graph e1, Graph e2) {
                            Integer a = e1.getDistance();
                            Integer b = e2.getDistance();

                            return a.compareTo(b);
                        }
                    })
                    .findFirst().get();

            graphToProcess = NextElementToProcess.getToNode();

            graphToProcess.setLocked(true);
            NextElementToProcess.setLocked(true);
        }

        /**
         * There is just the print
         */

        Graph graphToprint = lightestGraph;

        System.out.println("Distance total : " + graphToprint.getDistance());
        System.out.println("Node n° : |  x :  |  y :  ");
        System.out.println(" " + graphToprint.getToNode().getNbOfNode() + "\t\t " + (graphToprint.getToNode().getX() + 1)+ "\t\t " + (graphToprint.getToNode().getY() + 1));

        while (graphToprint.getParentId() != 0) {
            System.out.println(" " + graphToprint.getFromNode().getNbOfNode() + "\t\t " + (graphToprint.getFromNode().getX() + 1)+ "\t\t " + (graphToprint.getFromNode().getY() + 1));
            graphToprint = graphTotal.get(graphToprint.getParentId()-1);
        }

        System.out.println(" " + graphToprint.getFromNode().getNbOfNode() + "\t\t " + (graphToprint.getFromNode().getX() + 1)+ "\t\t " + (graphToprint.getFromNode().getY() + 1));
    }

    private static int[][] parseMatrixCharToInt(char[][] martix) {
        int maxY = martix.length, maxX = martix[0].length;

        int[][] intMatrix = new int[maxY][maxX];

        for (int y = 0; y < maxY; y++){
            for (int x = 0; x < maxX; x++){
                if (martix[y][x] == 'S') {
                    intMatrix[y][x] = 0;
                } else if (martix[y][x] == 'E') {
                    intMatrix[y][x] = 0;
                } else if((martix[y][x] == 'X')) {
                    intMatrix[y][x] = -1;
                } else{
                    intMatrix[y][x] = martix[y][x];
                }

                if (intMatrix[y][x] == 88) {
                    intMatrix[y][x] = 1;
                }
            }
        }

        return intMatrix;
    }

    private static List<Node> getNodes(int maxX, int maxY, int[][] intMatrix, char[][] charMatrix){
        int value, nbOfNode = -1;

        List<Node> nodes = new ArrayList<Node>();

        for (int y = 0; y < maxY+1; y++){
            for (int x = 0; x < maxX+1; x++){
                int nb = 0;
                int[] voisin = new int[4];
                nbOfNode++;
                value = intMatrix[y][x];

                if (x-1 >= 0 && intMatrix[y][x-1] > -1) {
                    voisin[nb] = nbOfNode-1;
                    nb++;
                }

                if (x+1 <= maxX && intMatrix[y][x+1] > -1) {
                    voisin[nb] = nbOfNode+1;
                    nb++;
                }

                if (y-1 >= 0 && intMatrix[y-1][x] > -1) {
                    voisin[nb] = nbOfNode - (maxX + 1);
                    nb++;
                }

                if (y+1 <= maxY && intMatrix[y+1][x] > -1) {
                    voisin[nb] = nbOfNode + maxX + 1;
                    nb++;
                }

                Node newNode = new Node(x, y, value, nbOfNode, voisin);

                if (charMatrix[y][x] == 'S'){
                    newNode.setStart(true);
                } else if(charMatrix[y][x] == 'E'){
                    newNode.setEnd(true);
                }

                nodes.add(newNode);
            }
        }

        return nodes;
    }

    /**
     * @param id : in C#, you need just to pass this var by address and increment it in the function
     */

    private static List<Graph> route(Node currenteNode, int distance, List<Node> nodes, Id id, Graph parent){
        List<Graph> localGraph = new ArrayList<Graph>();

        for (int neighbourid:
             currenteNode.getNeighbours()) {
            Node neighbour = nodes.get(neighbourid);
            int parentId;

            if (neighbour.isLocked()) {
                continue;
            }

            if(parent != null) {
                distance = parent.getDistance() + neighbour.getValue();
                parentId = parent.getId();
            } else {
                parentId = 0;
                distance = neighbour.getValue();
            }

            id.setId(id.getValue() + 1);

            Graph newGraph = new Graph(currenteNode, neighbour, distance, id.getValue(), parentId);

            Graph graphOfNeighbour = newGraph;

            localGraph.add(graphOfNeighbour);

        }

        if(localGraph.isEmpty()) {
            currenteNode.setLocked(true);
        }

        return localGraph;
    }

    public static Graph[] combine(Graph[] a, Graph[] b){
        int length = a.length + b.length;
        Graph[] result = new Graph[length];
        System.arraycopy(a, 0, result, 0, a.length);
        System.arraycopy(b, 0, result, a.length, b.length);
        return result;
    }
}
