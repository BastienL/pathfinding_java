/**
 * This method replace the "out" from C#
 */

public class Id {
    private int id;

    public int getValue() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
