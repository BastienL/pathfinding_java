public class Graph {
    private int distance;
    private Node fromNode;
    private Node toNode;
    private boolean locked;
    private int id;
    private int parentId;

    public Graph(Node fromNode, Node toNode, int distance, int id, int parentId) {
        this.fromNode = fromNode;
        this.distance = distance;
        this.toNode = toNode;
        this.id = id;
        this.parentId = parentId;
    }

    public Node getFromNode() {
        return fromNode;
    }

    public int getDistance() {
        return distance;
    }

    public Node getToNode() {
        return toNode;
    }

    public boolean isLocked() {
        return locked;
    }

    public void setLocked(boolean locked) {
        this.locked = locked;
    }

    public int getParentId() {
        return parentId;
    }

    public int getId() {
        return id;
    }
    public void whoIam(){
        System.out.println("i'm the graph with fromeNode : " + fromNode.getNbOfNode() + ", toNode " + toNode.getNbOfNode() + " and the distance " + distance + " locked : " + isLocked() + " My ID is : "+ this.getId());
    }
}